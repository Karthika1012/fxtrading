
package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.example.demo.business.Clientrepo;
import com.example.demo.business.Login;
import com.example.demo.business.Tradeprices;
import com.example.demo.business.Traderlist;
import com.example.demo.business.Transactions;
import com.example.demo.dao.MySQLJDBC;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class TestBusiness {
	@Autowired
	Clientrepo crepo;
	
	
	@Test
	public void checktrade() {
		List<Tradeprices> prices;
		List<Tradeprices> expected= new ArrayList <Tradeprices>();
		expected.add(new Tradeprices("T001","USD/INR","USD","INR",70.120000,70.170000));
		prices = crepo.gettradeprices("USD/INR");
		assertEquals(prices.get(0).getTraderID(),expected.get(0).getTraderID());
		assertEquals(prices.get(0).getBaseCurrency(),expected.get(0).getBaseCurrency());		
		assertEquals(prices.get(0).getTermCurrency(),expected.get(0).getTermCurrency());		
		assertTrue(prices.get(0).getBid()== 70.120000);
		assertTrue(prices.get(0).getOffer()== 70.170000);
	}
	
	@Test
	public void checklogin() {
		List<String> logins;
		MySQLJDBC obj = new MySQLJDBC();
		Login l = new Login("T003","P003");
		logins = obj.listProducts(l);
		assertEquals("Trader",logins.get(1));
	}
	
	
	@Test
	public void checklistcurrencypairs() {
		List<String> prices;
		prices = crepo.listOfCurrencyPair();		
		assertEquals("USD/INR",prices.get(0));
		assertEquals("USD/SGD",prices.get(2));
		assertEquals("USD/CNH",prices.get(4));
		
		}
	
	@Test
	public void checkupdate() {
		List<Tradeprices> update;
		Tradeprices t = new Tradeprices("T001","USD/INR","USD","INR",70.120000,70.170000);
		crepo.assigncurrencytotraders(t);
		update = crepo.gettradeprices("USD/INR");
		assertEquals("T001",update.get(0).getTraderID());		
	}
	
	

	@Test
	public void checkassignedcurrencypairs() {
		List<String> checkpairs;
		checkpairs = crepo.listofassignedcurrencypair();					
		assertEquals("USD/INR",checkpairs.get(0));		
		//assertEquals("GBP/USD",checkpairs.get(1));
		}
	
	@Test
	public void checkupdatebidofferprice() {
		List<Tradeprices> update;	
		crepo.setBidOfferPrice("T001","70.120000","70.170000");
		update = crepo.gettradeprices("USD/INR");
		assertTrue(update.get(0).getBid() == 70.120000);
		assertTrue(update.get(0).getOffer() == 70.170000);
		
	}
	
	
	@Test
	public void checkfinalrate() {
		assertTrue(crepo.getfinalrate("USD/INR","sell","USD",1000) == 70120);
				
		}
	

	@Test
	public void checktraderlist() {
		List<Traderlist> traderlist;
		traderlist = crepo.traderlist();	
		assertEquals("T001",traderlist.get(0).getTraderid());
		}
	
	@Test
	public void checkdeletetrader() {
		List<Tradeprices>  update2;
		Tradeprices t = new Tradeprices("T005","USD/CNH","USD","CNH",0.0000,0.0000);
		crepo.assigncurrencytotraders(t);
		List<Tradeprices> update;	
		crepo.setBidOfferPrice("T005","1.450000","1.680000");
		update = crepo.gettradeprices("USD/CNH");
		assertEquals("T005", update.get(0).getTraderID());
		assertTrue(update.get(0).getBid() == 1.45);
		assertTrue(update.get(0).getOffer() == 1.68);
		crepo.deletetraders("T005");
		update2 = crepo.gettradeprices("USD/CNH");
		assertTrue(update2.get(0).getBid() == 0.00);
		assertTrue(update2.get(0).getOffer() == 0.00);
		assertEquals("0",update2.get(0).getTraderID());
		}
	
	@Test
	public void checkiftraderpairassigned() {
		String success = crepo.checkcurrpair("USD/CNH");
		assertEquals("success",success);
		String failure = crepo.checkcurrpair("USD/INR");
		assertEquals("failure",failure);
		
	}
	
	@Test
	public void checkreassigntraderlist() {
		List<Tradeprices> update2;
		Tradeprices t = new Tradeprices("T005","EUR/USD","EUR","USD",0.0000,0.0000);
		crepo.assigncurrencytotraders(t);
		update2 = crepo.gettradeprices("EUR/USD");
		assertEquals("T005",update2.get(0).getTraderID());
	
		crepo.updatetraders("T005","GBP/USD");
		
		update2 = crepo.gettradeprices("GBP/USD");	
		assertEquals("T005",update2.get(0).getTraderID());
		assertEquals("GBP",update2.get(0).getBaseCurrency());		
		assertEquals("USD",update2.get(0).getTermCurrency());
	}
	
	@Test
	public void checklisttransaction() {
		List<Transactions> transaction;
		transaction=crepo.gettransactions("C001","Client");
		//transaction = crepo.gettradeprices("GBP/SGD");	
		assertEquals("4",transaction.get(0).getTid());
		assertEquals("AUD/USD",transaction.get(0).getCpair());
		assertEquals("buy",transaction.get(0).getIndicator());
		assertEquals("5",transaction.get(1).getTid());
		assertEquals("GBP/USD",transaction.get(1).getCpair());
		assertEquals("buy",transaction.get(1).getIndicator());
		
	}
	}

