package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
			 
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.example.demo.business.*;
import com.example.demo.dao.*;

@Controller
public class MyControllerThymeLeaf {
	private Logger logger =
		      LoggerFactory.getLogger(MyControllerThymeLeaf.class);

	@Autowired
	MySQLJDBC obj;
	@Autowired
	Clientrepo repocli; 

	@SuppressWarnings("deprecation")
	@RequestMapping("/")
	String homehandler(HttpSession session) {
		//session.removeAttribute("clientid");
		logger.info("Inside Homepage!");
		return "index";
	}
	@RequestMapping("/AboutUs")
	String aboutus(HttpSession session) {
		//session.removeAttribute("clientid");
		logger.info("Inside AboutUs!");
		return "about-us";
	}
	
	
	
	@RequestMapping("/test") 
	String testback(HttpServletRequest request, Model model, HttpSession session) {
		model.addAttribute("clientid", (String)session.getAttribute("clientid"));
		model.addAttribute("listofcurrencypair", repocli.listofassignedcurrencypair());
		return "clienthome";

	}


	@RequestMapping("/validate") 
	String logincheck(HttpServletRequest request, Model model, HttpSession session) {
		Login l = new Login(request.getParameter("userid"), request.getParameter("password"));
		List<String> res = obj.listProducts(l);
		if(!res.isEmpty()) {
			session.setAttribute("clientid", res.get(0));
			session.setAttribute("usertype", res.get(1));
			
			if((res.get(1)).equals("Client"))
			{   model.addAttribute("listofcurrencypair", repocli.listofassignedcurrencypair());
				model.addAttribute("clientid", (String)session.getAttribute("clientid"));
				logger.info("Inside Client Home!");
				return "clienthome";
			}
			if((res.get(1)).equals("Trader")) {
				model.addAttribute("clientid", (String)session.getAttribute("clientid"));
				String clientid = (String)session.getAttribute("clientid");
				String usertype = (String)session.getAttribute("usertype");
				model.addAttribute("trans", repocli.gettransactions(clientid, usertype));
				model.addAttribute("usertype", usertype);
				logger.info("Inside Trade Home!");
				return "transactionlist";
			}
			if ((res.get(1)).equals("Risk Manager")) {
				model.addAttribute("clientid", (String)session.getAttribute("clientid"));
				logger.info("Inside Risk Manager Home!");
				return "riskmanagerhome";
			}
		}			
		return "invalidcredentials";

	}
	@RequestMapping("/mapping")
	String mappingfunction(Model model, HttpSession session) {
		String result = repocli.tradercurrency((String)session.getAttribute("clientid"));
		if( result.equals("") ) {
			model.addAttribute("clientid", (String)session.getAttribute("clientid"));
			String clientid = (String)session.getAttribute("clientid");
			String usertype = (String)session.getAttribute("usertype");
			model.addAttribute("trans", repocli.gettransactions(clientid, usertype));
			model.addAttribute("usertype", usertype);
			model.addAttribute("flag", "1");
			model.addAttribute("msg"," Sorry ,The Currency pairs has not been set by the manager" );
			logger.info("Currency pair not set for trader!");
			return "traderhome";
		}
		else
		{
			model.addAttribute("hispair", result);
			model.addAttribute("hisname", (String)session.getAttribute("clientid"));
			//model.addAttribute("")
			String res=repocli.gettradersbidoffer( (String)session.getAttribute("clientid"));
			String[] arr = res.split("-");
			model.addAttribute("bid",arr[0]);
			model.addAttribute("offer",arr[1]);	
			logger.info("Set bid and offer price for currency pair!");
			//model.addAttribute("res",res);
			return "dealpricing";	
		}
	}



	@RequestMapping("/transactionsmapping")
	String displaytransactions(Model model, HttpSession session) {

		if(session.getAttribute("clientid")!=null) {
			String clientid = (String)session.getAttribute("clientid");
			String usertype = (String)session.getAttribute("usertype");
			model.addAttribute("trans", repocli.gettransactions(clientid, usertype));
			model.addAttribute("usertype", usertype);
			if(usertype.equals("Client")) {
				logger.info("Cleint-Transactions page!");
				return "transactionclient";
			}
			else if(usertype.equals("Trader")){
				logger.info("Trader-Transactions page!");
				return "transactionlist"; //any problem arises change it into traderhome		
			}
			else  {
				logger.info("Risk Manager-Comeplete Transactions page!");
				return "fulltransactions";
			}
		}		
		else {
			return "index";
		}
	}
	@RequestMapping("/pricecurrency")
	String updateprice(Model model, HttpSession session, HttpServletRequest request) {
		String userid = (String)session.getAttribute("clientid"); 
		String bidprice = request.getParameter("bidrate");
		String offerprice = request.getParameter("offerrate");	
		//String clientid = (String)session.getAttribute("clientid");
		String usertype = (String)session.getAttribute("usertype");
		String result = repocli.setBidOfferPrice(userid, bidprice, offerprice);
		if(result == "success") {
			model.addAttribute("flag", "2");
			model.addAttribute("msg", "The Currency pairs has been successfully updated " );
			model.addAttribute("trans", repocli.gettransactions(userid, usertype));
			logger.info("Trader-Updating bid and offer price!");
			return "traderhome";
		}
		return "index";
	}

	@RequestMapping("/assigncurrencytotraders")
	String assigningcurrency(HttpSession session, Model model) {
		if(session.getAttribute("clientid")!=null && session.getAttribute("usertype").equals("Risk Manager")) {
			model.addAttribute("unassignedtraderlist",repocli.listofunassignedtraders());
			model.addAttribute("listofcurrencypair", repocli.listOfCurrencyPair());
			model.addAttribute("listofassignedcurrencypair", repocli.listofassignedcurrencypair());
			logger.info("Risk Manager-Assigninmg trader paird to traders!");
			return "assigncurrencypair";
		}		
		return "index";
	}

	@RequestMapping("/assign") 
	String traderassigncurrency(HttpServletRequest request, Model model, HttpSession session) {

		if(session.getAttribute("clientid")!=null && session.getAttribute("usertype").equals("Risk Manager")) {
			String tid=request.getParameter("tid");
			String currencypair=request.getParameter("currencypair");
			Tradeprices tp = new Tradeprices(tid, currencypair,"","",0.0,0.0);
			int  res=repocli.assigncurrencytotraders(tp);
			if(res == 1) {
				model.addAttribute("tradersandpairs",repocli.traderlist());
				logger.info("Risk Manager-Updating traders currency pair!");
				return "tradersandcpair";
			}

		}
		return "index";

	}


	@RequestMapping("/bookdeal")
	public String bookhandler(HttpServletRequest request,Model model, HttpSession session) {       	
		model.addAttribute("pair",repocli.gettradeprices(request.getParameter("CurrencyPairs")));
		session.setAttribute("currencypair", request.getParameter("CurrencyPairs"));
		model.addAttribute("cpair",request.getParameter("CurrencyPairs"));
		logger.info("Client choosing currency pair!");
		return "bookdeal1";
	}
	@RequestMapping("/getrate")
	public String printcurrency(HttpServletRequest request,Model model,HttpSession session) {    
		model.addAttribute("finalrate",repocli.getfinalrate((String)session.getAttribute("currencypair"),request.getParameter("buysell"),
				request.getParameter("currency"),Double.parseDouble(request.getParameter("amount"))));
		session.setAttribute("dbuysell", request.getParameter("buysell"));
		session.setAttribute("dcurrency", request.getParameter("currency"));
		session.setAttribute("damount", Double.parseDouble(request.getParameter("amount")));
		logger.info("Display and get final rate for chosen currency pair!");
		return "getrate1";
	}
	@RequestMapping("/add1")
	public String addclientdata(HttpServletRequest request,Model model,HttpSession session) {  
		//session.setAttribute("dfinalrate", Double.parseDouble(request.getParameter("finalrate")));
		String clientid = (String)session.getAttribute("clientid");
		String usertype = (String)session.getAttribute("usertype");
		session.setAttribute("dbuysell", request.getParameter("buysell"));
		session.setAttribute("dcurrency", request.getParameter("currency"));
		session.setAttribute("finalresult", Double.parseDouble(request.getParameter("finalresult")));
		session.setAttribute("amount", Double.parseDouble(request.getParameter("amount")));
		Tradeprices getrate3 = repocli.getupdatetransaction((String)session.getAttribute("clientid"),
				(String)session.getAttribute("currencypair"),
				(String)session.getAttribute("dbuysell"),
				(String)session.getAttribute("dcurrency"),
				((Double)session.getAttribute("finalresult")), ((Double)session.getAttribute("amount")),request.getParameter("day"));
		model.addAttribute("trans", repocli.gettransactions(clientid, usertype));
		model.addAttribute("usertype", usertype);
		//model.addAttribute("getrate3",getrate3);
		//model.addAttribute("Hello", (Double)session.getAttribute("damount"));
		logger.info("Adding transaction to the table!");
		return "transactionclient";
	}
	@RequestMapping("/tradercurrencypairs")
	String gettraderdetails(Model model,HttpSession session)
	{
		if(session.getAttribute("clientid")!=null && session.getAttribute("usertype").equals("Risk Manager"))
		{
			model.addAttribute("tradersandpairs",repocli.traderlist());
			logger.info("Risk Manager view of trader and currency pair!");
			return "tradersandcpair";
		}
		return "index";
	}

	@RequestMapping("/deletetrader/{id}")
	String deletetraderhandler(@PathVariable("id") String id,Model model) {
		//model.addAttribute("shipper", srepo.getShipper(Integer.parseInt(id))); 
		int res=repocli.deletetraders((id));
		if(res != 0) {	
			model.addAttribute("tradersandpairs",repocli.traderlist());
			logger.info("Delete trader pair!");
			return "tradersandcpair";
		}
		return "index";
	}

	@RequestMapping("/updatetrader/{id}")
	String updatetraderhandler(HttpSession session,@PathVariable("id") String id,Model model) {

		if(session.getAttribute("clientid")!=null && session.getAttribute("usertype").equals("Risk Manager"))
		{
			model.addAttribute("traderid",id);
			//model.addAttribute("cpair",cpair);
			model.addAttribute("listofcurrencypair", repocli.listOfCurrencyPair());
			logger.info("Risk Manager updates currency pair of trader!");
			return "updatetraderform";
		}
		return "index";
	}

	@RequestMapping("/save")
	public String savenewcpairhandler(HttpServletRequest request,Model model, HttpSession session) {       

		if(session.getAttribute("clientid")!=null && session.getAttribute("usertype").equals("Risk Manager"))
		{
			int res=repocli.updatetraders(request.getParameter("traderid"),request.getParameter("currencypair"));
			if(res != 0) {	
				model.addAttribute("tradersandpairs",repocli.traderlist());
				logger.info("Update Saved!");
				return "tradersandcpair";
			}
		}
		return "index";
	}

	@RequestMapping("/traderback")
	public String traderbackhandler(Model model,HttpSession session)
	{
		String userid = (String)session.getAttribute("clientid"); 
		String usertype = (String)session.getAttribute("usertype"); 
		model.addAttribute("trans", repocli.gettransactions(userid, usertype));
		logger.info("Trader back to homepage!");
		return "traderhome";
	}
	
	@RequestMapping("/managerback")
	public String managerbackhandler()
	{
		/*String userid = (String)session.getAttribute("clientid"); 
		String usertype = (String)session.getAttribute("usertype"); 
		model.addAttribute("trans", repocli.gettransactions(userid, usertype));*/
		logger.info("Risk Manager back to homepage!");
		return "riskmanagerhome";
	}
	
	@RequestMapping("/clientback")
	public String clientbackhandler(Model model,HttpSession session)
	{
		model.addAttribute("clientid", (String)session.getAttribute("clientid"));
		model.addAttribute("listofcurrencypair", repocli.listofassignedcurrencypair());
		logger.info("Client back to homepage!");
		return "clienthome";
	}
	
	@RequestMapping("/managerupdateback")
	public String managerupdatebackhandler(Model model,HttpSession session)
	{
		model.addAttribute("tradersandpairs",repocli.traderlist());
		logger.info("Manager cancels update!");
		return "tradersandcpair";
	}

	
}

