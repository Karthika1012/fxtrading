package com.example.demo.business;

public class Traderlist {
	private String traderid;
	private String cpair;
	private String base;
	private String term;
	private double bid;
	private double offer;
	public Traderlist(String traderid, String cpair, String base, String term, double bid, double offer) {
		super();
		this.traderid = traderid;
		this.cpair = cpair;
		this.base = base;
		this.term = term;
		this.bid = bid;
		this.offer = offer;
	}
	public String getTraderid() {
		return traderid;
	}
	public void setTraderid(String traderid) {
		this.traderid = traderid;
	}
	public String getCpair() {
		return cpair;
	}
	public void setCpair(String cpair) {
		this.cpair = cpair;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public double getBid() {
		return bid;
	}
	public void setBid(double bid) {
		this.bid = bid;
	}
	public double getOffer() {
		return offer;
	}
	public void setOffer(double offer) {
		this.offer = offer;
	}
	
}
