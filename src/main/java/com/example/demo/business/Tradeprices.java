package com.example.demo.business;

public class Tradeprices {
String TraderID, CPair, BaseCurrency, TermCurrency;
double Bid, Offer;

public Tradeprices(String traderID, String cPair, String baseCurrency, String termCurrency, double bid, double offer) {
	super();
	TraderID = traderID;
	CPair = cPair;
	BaseCurrency = baseCurrency;
	TermCurrency = termCurrency;
	Bid = bid;
	Offer = offer;
}

public String getTraderID() {
	return TraderID;
}

public void setTraderID(String traderID) {
	TraderID = traderID;
}

public String getCPair() {
	return CPair;
}

public void setCPair(String cPair) {
	CPair = cPair;
}

public String getBaseCurrency() {
	return BaseCurrency;
}

public void setBaseCurrency(String baseCurrency) {
	BaseCurrency = baseCurrency;
}

public String getTermCurrency() {
	return TermCurrency;
}

public void setTermCurrency(String termCurrency) {
	TermCurrency = termCurrency;
}

public double getBid() {
	return Bid;
}

public void setBid(double bid) {
	Bid = bid;
}

public double getOffer() {
	return Offer;
}

public void setOffer(double offer) {
	Offer = offer;
}

}
