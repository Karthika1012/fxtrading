package com.example.demo.business;

import java.util.List;

public interface Clientinterface {
	List<Transactions> ListTransactions(String clientid, String usertype);
	List<String> listOfUnassignedTraders();
	int AssignCurrencytoTraders(Tradeprices t);
	public List<String> listOfCurrencyPair();
	public List<String>listofassignedcurrencypair();
	String setBidOfferPrice(String userid, String bidprice, String offerprice);
	List<Tradeprices> ListTradePrices(String pair);
	double ListFinalRate(String cpair,String buysell,String currency,double amount);
	List<Traderlist> gettraderlist();
	int deletingtrader(String id);
	String tradercurrency(String clientid);
	Tradeprices ListUpdateTransaction(String clientid, String cpair,String buysell,String currency,double price, double amount, String date);
	String checkcurrassigned(String cpair);
	int doupdatetraders(String traderid, String cpair);
	String getbidoffer(String currencypair, String buysell, String currency, String amount);
	String listtraderbidoffer(String result);

	/*List<Tradeprices> ListTradePrices(String pair);
	String ListBaseCurrency(String base);
	double ListFinalRate(String cpair,String buysell,String currency,double amount);*/
}
