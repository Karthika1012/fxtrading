package com.example.demo.business;
import java.util.Date;

public class Transactions {
	private String tid, clientid,traderid, cpair;
	private String  dealcurrency, indicator;
	private double price, dealamount;
	private Date tradedate;
	private String valuedate;
	private double finalamount;
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getClientid() {
		return clientid;
	}
	public void setClientid(String clientid) {
		this.clientid = clientid;
	}
	public String getTraderid() {
		return traderid;
	}
	public void setTraderid(String traderid) {
		this.traderid = traderid;
	}
	public String getCpair() {
		return cpair;
	}
	public void setCpair(String cpair) {
		this.cpair = cpair;
	}
	
	public double getFinalamount() {
		return finalamount;
	}
	public void setFinalamount(double finalamount) {
		this.finalamount = finalamount;
	}
	public String getDealcurrency() {
		return dealcurrency;
	}
	public void setDealcurrency(String dealcurrency) {
		this.dealcurrency = dealcurrency;
	}
	public String getIndicator() {
		return indicator;
	}
	public void setIndicator(String indicator) {
		this.indicator = indicator;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getDealamount() {
		return dealamount;
	}
	public void setDealamount(double dealamount) {
		this.dealamount = dealamount;
	}
	public Date getTradedate() {
		return tradedate;
	}
	public void setTradedate(Date tradedate) {
		this.tradedate = tradedate;
	}
	public String getValuedate() {
		return valuedate;
	}
	public void setValuedate(String valuedate) {
		this.valuedate = valuedate;
	}
	public Transactions(String tid, String clientid, String traderid, String cpair,
			String dealcurrency, String indicator, double price, double dealamount,
			Date tradedate, String valuedate, double finalamount) {
		super();
		this.tid = tid;
		this.clientid = clientid;
		this.traderid = traderid;
		this.cpair = cpair;
		this.dealcurrency = dealcurrency;
		this.indicator = indicator;
		this.price = price;
		this.dealamount = dealamount;
		this.tradedate = tradedate;
		this.valuedate = valuedate;
		this.finalamount = finalamount;
	}
	
	
	
}