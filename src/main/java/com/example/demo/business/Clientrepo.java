package com.example.demo.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Clientrepo {
	@Autowired
	Clientinterface cli;
	public List<Transactions> gettransactions(String clientid, String usertype) {
		return cli.ListTransactions(clientid, usertype);
	}
	public List<String> listofunassignedtraders() {
		return cli.listOfUnassignedTraders();
	}
	
	public int assigncurrencytotraders(Tradeprices t) {
		return cli.AssignCurrencytoTraders(t);
	}
	public List<String> listOfCurrencyPair() {
		return cli.listOfCurrencyPair();
	}
	public List<String>listofassignedcurrencypair(){
		return cli.listofassignedcurrencypair();
	}
	public String setBidOfferPrice(String userid, String bidprice, String offerprice) {
		return cli.setBidOfferPrice(userid, bidprice, offerprice);
	}
	public List<Tradeprices> gettradeprices(String pair){
		return cli.ListTradePrices(pair);
	}
	public double getfinalrate(String cpair,String buysell,String currency,double amount){
		return cli.ListFinalRate(cpair,buysell,currency,amount);
	}	
	public List<Traderlist> traderlist() {
		return cli.gettraderlist();
	}
	public int deletetraders(String id) {
		return cli.deletingtrader(id);
	}
	public String tradercurrency(String clientid) {
		return cli.tradercurrency(clientid);
	}
	public Tradeprices getupdatetransaction(String clientid, String cpair,String buysell,String currency,double price, double amount, String date){
		return cli.ListUpdateTransaction(clientid, cpair,buysell,currency,price,amount, date);
	}
	public String checkcurrpair(String cpair) {
		return cli.checkcurrassigned(cpair);
	}
	public int updatetraders(String traderid, String cpair) {
		// TODO Auto-generated method stub
		return cli.doupdatetraders(traderid,cpair);
	}
	public String getrate(String currencypair, String buysell, String currency, String amount) {
		// TODO Auto-generated method stub
		return cli.getbidoffer(currencypair,buysell,currency,amount);
	}
	public String gettradersbidoffer(String result) {
		// TODO Auto-generated method stub
		return cli.listtraderbidoffer(result);
	}
	

}
