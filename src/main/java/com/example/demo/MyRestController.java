package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.business.Clientrepo;

@RestController
public class MyRestController {
	
	@Autowired
	Clientrepo repocli; 
	
	@RequestMapping(method=RequestMethod.GET, value="/cpair", headers="Accept=application/json")
	public  String checkpair(@RequestParam String currencypair){
	    return  repocli.checkcurrpair(currencypair);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/assigncpair", headers="Accept=application/json")
	public  String assignpair(@RequestParam String currencypair){
	    return  repocli.checkcurrpair(currencypair);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/cpairs",headers="Accept=application/json")
	public String getrates(@RequestParam String currencypair,@RequestParam String buysell,@RequestParam String currency,@RequestParam String amount){
	    //System.out.println(currencypair+"hello"+buysell+"hello"+currency+"hello"+amount);
		//String amount="10000";
		String res= repocli.getrate(currencypair,buysell,currency,amount);
		System.out.println("Passing res"+res);
		return res;
	}

}
