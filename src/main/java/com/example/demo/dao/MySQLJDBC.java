package com.example.demo.dao;

import java.sql.*;
import java.util.ArrayList;
//import java.util.ArrayList;
//import java.util.List;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.business.*;

@Component
public class MySQLJDBC implements Clientinterface {
	@Override
	public List<Tradeprices> ListTradePrices(String pair){
		List<Tradeprices> prices = new ArrayList<>();		
		ResultSet rs = null;
		Connection cn = null;
		try{
			
			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = (Connection) DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement st = (PreparedStatement) cn.prepareStatement("SELECT * from tradeprice where cpair =?");
			st.setString(1, pair);
			rs = st.executeQuery();
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				prices.add( new Tradeprices(rs.getString(1), rs.getString(2),
						rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6)));
			}
			st.close();
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return prices;
	}



	public List<String> listProducts(Login l){

		ResultSet rs = null;
		Connection cn = null;
		List<String> result = new ArrayList<String>();

		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");//change later
			PreparedStatement ps = cn.prepareStatement("SELECT * from loginuser WHERE UserID = ? AND UPassword = ?");
			ps.setString(1, l.getUserid());
			ps.setString(2, l.getPassword());
			rs = ps.executeQuery();

			if (rs != null) {			 
				if(rs.next()) {			
					result.add(rs.getString(1));
					result.add(rs.getString(3));
					//System.out.println("Result value"+result.get(0)+"\n"+result.get(1));
				}			 
			}
			ps.close();
		}


		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return result;
	}

	@Override

	public List<Transactions> ListTransactions(String clientid, String usertype) {
		ResultSet rs = null;
		Connection cn = null;
		List<Transactions> result = new ArrayList<>(); 
		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			//PreparedStatement ps;
			String query = "{CALL transaction_procedure(?, ?)}";
			CallableStatement stmt = cn.prepareCall(query);
			if(usertype.equals("Client")) {
				stmt.setInt(1, 1);
				stmt.setString(2, clientid);
				rs = stmt.executeQuery();

			}
			else if(usertype.equals("Trader")) {
				stmt.setInt(1, 2);
				stmt.setString(2, clientid);
				
				
				//ps.setString(1, clientid);
				rs = stmt.executeQuery();
			}
			else {
				stmt.setInt(1, 3);
				stmt.setString(2, clientid);
				//ps = cn.prepareStatement("SELECT * From transactiondetails");
				rs = stmt.executeQuery();

			}
			

			while(rs.next()) {			
				result.add(new Transactions(rs.getString(1), rs.getString(2), rs.getString(3),
						rs.getString(4), rs.getString(5), rs.getString(6),rs.getDouble(7),
						rs.getDouble(8),rs.getDate(9), rs.getString(10), rs.getDouble(11))) ;							 
			}			 

		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return result;

	}

	@Override
	public List<String> listOfUnassignedTraders() {
		ResultSet rs = null;
		Connection cn = null;
		List<String> result = new ArrayList<>(); 

		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");//change later
			PreparedStatement ps;

			ps = cn.prepareStatement("SELECT UserID FROM loginuser WHERE UserType = 'Trader' AND  UserID NOT IN(SELECT TraderID FROM tradeprice)");
			rs = ps.executeQuery();
			while(rs.next()) {			
				result.add(rs.getString(1)) ;							 
			}
			
			ps.close();
		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return result;
	}
	@Override
	public List<String> listOfCurrencyPair() {
		ResultSet rs = null;
		Connection cn = null;
		List<String> result = new ArrayList<>(); 

		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");//change later
			PreparedStatement ps;

			ps = cn.prepareStatement("SELECT cpair FROM tradeprice");
			rs = ps.executeQuery();
			while(rs.next()) {			
				result.add(rs.getString(1)) ;							 
			}
			ps.close();

		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return result;
	}


	@Override
	public int AssignCurrencytoTraders(Tradeprices t) {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		Connection cn = null;
		//List<String> result = new ArrayList<>(); 
		int rows = 0;
		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");//change later
			PreparedStatement ps;
			ps = cn.prepareStatement("UPDATE tradeprice set traderid = ? where cpair = ?");
			ps.setString(1,t.getTraderID());
			ps.setString(2,t.getCPair());
			
			//add insert command
			 rows = ps.executeUpdate();			
			ps.close();
		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return rows;
		
	}
	public List<String> listofassignedcurrencypair() {
		
		ResultSet rs = null;
		Connection cn = null;
		List<String> result = new ArrayList<>(); 
		//int rows = 0;
		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");//change later
			PreparedStatement ps;

			ps = cn.prepareStatement("SELECT cpair from tradeprice where traderid != '0' AND bid != 0.0 AND Offer != 0.0 ");
			//add insert command
			 rs= ps.executeQuery();	
			 while(rs.next()) {			
					result.add(rs.getString(1)) ;							 
				}
			 ps.close();
		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return result;
		
	}
	public String setBidOfferPrice(String userid, String bidprice, String offerprice) {
		String result = "";
		ResultSet rs = null;
		Connection cn = null;
		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement ps;
			ps = cn.prepareStatement("Update tradeprice set bid = ?, offer = ? where traderid = ? ");
			ps.setDouble(1, Double.parseDouble(bidprice));
			ps.setDouble(2, Double.parseDouble(offerprice));
			ps.setString(3, userid);
			int rows = ps.executeUpdate();
			if(rows == 1 ) {
				return "success";
			}
			ps.close();
		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		
	return result;
	}
	@Override
	public double ListFinalRate(String cpair,String buysell, String currency, double amount) {
		// TODO Auto-generated method stub
		Tradeprices traderate = null;		
		ResultSet rs = null;
		Connection cn = null;
		double finalamount = 0;
		try{
			
			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = (Connection) DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement st = (PreparedStatement) cn.prepareStatement("SELECT * from tradeprice where cpair =?");
			st.setString(1, cpair);
			rs = st.executeQuery();		
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				traderate = new Tradeprices(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6));				
			}
			
			if((currency.equals(traderate.getBaseCurrency())) && (buysell.equals("buy"))) {
				finalamount = amount / traderate.getOffer();
			}
			else if((currency.equals(traderate.getBaseCurrency())) && (buysell.equals("sell"))) {
				finalamount = amount * traderate.getBid();
			}
			else if(currency.equals(traderate.getTermCurrency()) && (buysell.equals("buy"))) {
				finalamount = amount * traderate.getBid();
			}
			else if(currency.equals(traderate.getTermCurrency()) && (buysell.equals("sell"))) {
				finalamount = amount / traderate.getOffer();
			}
			else {
				finalamount = 5000;
			}
			st.close();			
			
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
			

		return finalamount;
	}
	@Override
	public List<Traderlist> gettraderlist() {
		
		ResultSet rs = null;
		Connection cn = null;
		List<Traderlist> result = new ArrayList<>(); 
		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement ps;
			ps = cn.prepareStatement("SELECT * From tradeprice where traderid not like '0'");
			rs = ps.executeQuery();
			while(rs.next()) {			
				result.add(new Traderlist(rs.getString(1), rs.getString(2), rs.getString(3),
						rs.getString(4),rs.getDouble(5), rs.getDouble(6))) ;							 
			}
			ps.close();

		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return result;
	}


	@Override
	public int deletingtrader(String id) {
		int rows=0;
		ResultSet rs = null;
		Connection cn = null;
		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement ps;
			ps = cn.prepareStatement("Update tradeprice set Traderid = '0', bid = 0.0, offer = 0.0 where traderid = ? ");
			ps.setString(1, id);
			rows = ps.executeUpdate();
			ps.close();
		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		
	return rows;
	
	}
	public String tradercurrency(String clientid) {
		String result="";
		ResultSet rs = null;
		Connection cn = null;
		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement ps;
			ps = cn.prepareStatement("Select cpair from tradeprice where traderid=?");
			ps.setString(1, clientid);
			rs = ps.executeQuery();
			if(rs.next()) {
				result=rs.getString(1);
				
			}
			ps.close();
		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}		
		return result;
	}

	
	
	
	public Tradeprices ListUpdateTransaction(String clientid, String cpair, String buysell, String currency,double price,double amount,String date){
		Tradeprices updated = null;		
		ResultSet rs = null;
		Connection cn = null;
		double priced = 0;
		int rows =0;
		try{
			
			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = (Connection) DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement st = (PreparedStatement) cn.prepareStatement("SELECT * from tradeprice where cpair =?");
			st.setString(1, cpair);
			rs = st.executeQuery();
			
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				updated = new Tradeprices(rs.getString(1), rs.getString(2),
						rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6));
			}
			
			PreparedStatement str = (PreparedStatement) cn.prepareStatement("INSERT INTO transactiondetails (ClientID, TraderID, Cpair, DealCurrency, Indicator, Price, DealAmount, TradeDate, ValueDate, finalamount ) values (?,?,?,?,?,?,?,?,?,?)");
			str.setString(1, clientid);
			str.setString(2,updated.getTraderID());
			str.setString(3,updated.getCPair());
			str.setString(4,currency);
			str.setString(5,buysell);
			if((currency.equals((updated).getBaseCurrency())) && (buysell.equals("buy"))) {
				priced = (updated).getOffer();
			}
			else if((currency.equals((updated).getBaseCurrency())) && (buysell.equals("sell"))) {
				priced =(updated).getBid();
			}
			else if((currency.equals((updated).getTermCurrency())) && (buysell.equals("buy"))) {
				priced =updated.getBid();
			}
			else if((currency.equals((updated).getTermCurrency())) && (buysell.equals("sell"))) {
				priced =( updated).getOffer();
			}
			else {
				priced = 0;
			}
			str.setDouble(6, priced);
			//double dealamount = amount*priced;
			str.setDouble(7, amount);
			
			
			java.util.Date utilDate = new java.util.Date();
		    java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		    
			str.setDate(8,sqlDate);
			str.setString(9, date);
			str.setDouble(10,price);
			
			
			rows = str.executeUpdate();
			st.close();
			str.close();
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return updated;
}
	
	
	
	@Override
	public String checkcurrassigned(String cpair) {
		// TODO Auto-generated method stub
		int count=0;
		ResultSet rs = null;
		Connection cn = null;
		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement ps;
			ps = cn.prepareStatement("Select count(*) from tradeprice where cpair= ? and  Traderid = '0' ");
			ps.setString(1, cpair);
			//System.out.println("HELLO"+cpair);
			rs = ps.executeQuery();
			if(rs.next())
			{
				count=rs.getInt(1);
			}
			ps.close();
		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		if(count==1) {
			//System.out.println("RESULT : YOU CAN INSERT THIS "+count);
			return "success";
		}
		else {
			//System.out.println("RESULT : YOU CANNOT INSERT THIS "+count);
			return "failure";
		}
		
	}



	@Override
	public int doupdatetraders(String traderid, String cpair) {
		// TODO Auto-generated method stub
		int rows=0,count=0;
		//ResultSet rs = null;
		Connection cn = null;
		try{

			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement ps,ps2;
			ps = cn.prepareStatement("Update tradeprice set Traderid = '0', bid = 0.0, offer = 0.0 where traderid = ? ");
			ps.setString(1, traderid);
			rows = ps.executeUpdate();
			//System.out.println("RESULT : tRADER Details Reset "+rows + traderid);
			ps2 = cn.prepareStatement("Update tradeprice set Traderid = ? where CPair = ? ");
			ps2.setString(1, traderid);
			ps2.setString(2, cpair);
			count = ps2.executeUpdate();
			ps.close();
			ps2.close();
			//System.out.println("RESULT : New Details set " + traderid + count +cpair);
		}		

		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		if(rows == 1 && count == 1 ) {
			return rows;
		}
		return 0;
	}
	@Override
	public String getbidoffer(String currencypair, String buysell, String currency,String amount) {
		// TODO Auto-generated method stub
		//Tradeprices traderate = null;		
		ResultSet rs = null;
		Connection cn = null;
		double finalamount = 0;
		double price=0.0;
		double transamount=Double.parseDouble(amount);
		String result=null;
		System.out.println("hello"+currencypair+buysell+currency+transamount);
		try{
			
			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = (Connection) DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			
			PreparedStatement ps=cn.prepareStatement("Select basecurrency,termcurrency,bid,offer from tradeprice where cpair=?");
			ps.setString(1,currencypair);
			rs=ps.executeQuery();
			if(rs.next())
			{
			    String base=rs.getString(1);
			    String term=rs.getString(2);
			    double bid=rs.getDouble(3);
			    double offer=rs.getDouble(4);
			    if(currency.equals(base) && buysell.equals("buy")){
			        price=offer;
			        finalamount=transamount/price;
			    }
			    if(currency.equals(base) && buysell.equals("sell")){
			        price=bid;
			        finalamount=transamount*price;
			    }
			    if(currency.equals(term) && buysell.equals("buy")){
			        price=bid;
			        finalamount=transamount*price;
			    }
			    if(currency.equals(term) && buysell.equals("sell")){
			        price=offer;
			        finalamount=transamount/price;
			    }
			}
			result=Double.toString(price)+" "+Double.toString(finalamount);
			//finalamount=price;
			ps.close();
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		//System.out.println(rs.getString(1)+rs.getString(2)+ rs.getString(3)+ rs.getString(4)+ rs.getDouble(5)+ rs.getDouble(6));
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
			
		System.out.println(finalamount+result);
		//return finalamount;
		return result;
		 
	}



	@Override
	public String listtraderbidoffer(String id) {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		Connection cn = null;
		String res="";
		try{
			
			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = (Connection) DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement st = (PreparedStatement) cn.prepareStatement("SELECT bid,offer from tradeprice where TraderID =?");
			st.setString(1, id);
			rs = st.executeQuery();
			// Process the results of the query, one row at a time
			if (rs.next()) { 
				res=rs.getDouble(1)+"-"+rs.getDouble(2);
			}
			st.close();
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return res;
	}

}




